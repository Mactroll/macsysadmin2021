{	{{ if .Token.email }}
		"subject": {{ toJson .Token.email }},
		"sans": [{"type": "email", "value": {{ toJson .Token.email }} }],
	{{ else }}
		"subject: {{ toJson .Subject }},
	{{ end }}
}
