# MacSysAdmin2021



## Supporting files for my MacSysAdmin 2021 Session

Here you will find the nginx, oauth2-proxy and SmallStep configuration files to get you dangerous with creating your own DIY ZTNA setup.

## Hosts

<p>Sample hosts used in config files:</p>

<ul>
<li>cert.example.com - SmallStep server issuing certificates, fronted by nginx running as a reverse proxy</li>
<li>run.exmaple.com - nginx running as a reverse proxy using OAuth2-proxy for authentication</li>
<li>auth.example.com - nginx running as a reverse proxy using certificate authentication
</li>
</ul>
